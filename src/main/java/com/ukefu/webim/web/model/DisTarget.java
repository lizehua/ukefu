package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.List;

public class DisTarget implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8146810678920578657L;

	private List<String> useridList ;
	private List<String> organidList ;
	public List<String> getUseridList() {
		return useridList;
	}
	public void setUseridList(List<String> useridList) {
		this.useridList = useridList;
	}
	public List<String> getOrganidList() {
		return organidList;
	}
	public void setOrganidList(List<String> organidList) {
		this.organidList = organidList;
	}
	
}
