package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.List;

public class FilterCriteria implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7353795681040849124L;
	
	private String q ;
	private String ckind ;
	private String itemid ;
	private String name ;
	private String creater ;
	private String ckinds ;
	private String gender ;
	private String phone ;
	private String mobileno ;
	private String email ;
	private String address ;
	private String province ;
	private String city ;
	private String ctype ;
	private String touchtimebegin ;
	private String touchtimeend ;
	
	private String cusname ;//客户名称
	private String cusphone ;//客户-联系电话
	private String cusemail ;//客户-电子邮件
	private String cusprovince ;//客户-省份
	private String cuscity ;//客户-城市
	private String cusaddress ;//客户-客户地址
	private String ekind ;//客户-客户类型
	private String elevel ;//客户-客户级别
	private String esource ;//客户-客户来源
	private String maturity ;//客户-成熟度
	private String industry ;//客户-行业
	private String cusvalidstatus ;//客户-客户状态
	
	private List<String> contactsids ;
	
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		this.q = q;
	}
	public String getCkind() {
		return ckind;
	}
	public void setCkind(String ckind) {
		this.ckind = ckind;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCtype() {
		return ctype;
	}
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}
	public String getTouchtimebegin() {
		return touchtimebegin;
	}
	public void setTouchtimebegin(String touchtimebegin) {
		this.touchtimebegin = touchtimebegin;
	}
	public String getTouchtimeend() {
		return touchtimeend;
	}
	public void setTouchtimeend(String touchtimeend) {
		this.touchtimeend = touchtimeend;
	}
	public String getCkinds() {
		return ckinds;
	}
	public void setCkinds(String ckinds) {
		this.ckinds = ckinds;
	}
	public List<String> getContactsids() {
		return contactsids;
	}
	public void setContactsids(List<String> contactsids) {
		this.contactsids = contactsids;
	}
	public String getCusname() {
		return cusname;
	}
	public void setCusname(String cusname) {
		this.cusname = cusname;
	}
	public String getCusphone() {
		return cusphone;
	}
	public void setCusphone(String cusphone) {
		this.cusphone = cusphone;
	}
	public String getCusemail() {
		return cusemail;
	}
	public void setCusemail(String cusemail) {
		this.cusemail = cusemail;
	}
	public String getCusprovince() {
		return cusprovince;
	}
	public void setCusprovince(String cusprovince) {
		this.cusprovince = cusprovince;
	}
	public String getCuscity() {
		return cuscity;
	}
	public void setCuscity(String cuscity) {
		this.cuscity = cuscity;
	}
	public String getCusaddress() {
		return cusaddress;
	}
	public void setCusaddress(String cusaddress) {
		this.cusaddress = cusaddress;
	}
	public String getEkind() {
		return ekind;
	}
	public void setEkind(String ekind) {
		this.ekind = ekind;
	}
	public String getElevel() {
		return elevel;
	}
	public void setElevel(String elevel) {
		this.elevel = elevel;
	}
	public String getEsource() {
		return esource;
	}
	public void setEsource(String esource) {
		this.esource = esource;
	}
	public String getMaturity() {
		return maturity;
	}
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getCusvalidstatus() {
		return cusvalidstatus;
	}
	public void setCusvalidstatus(String cusvalidstatus) {
		this.cusvalidstatus = cusvalidstatus;
	}
	

}
