package com.ukefu.webim.web.model;

public class AiMessageMapping implements java.io.Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String keyword ;
	private String skill;
	
	public AiMessageMapping() {}
	
	public AiMessageMapping(String keyword, String skill) {
		this.keyword = keyword ;
		this.skill = skill ;
	}
	public String getKeyword() {
		return keyword;
	}
	public String getSkill() {
		return skill;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
}
