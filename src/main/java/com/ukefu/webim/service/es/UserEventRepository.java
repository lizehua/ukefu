package com.ukefu.webim.service.es;


import java.util.List;

import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.UserHistory;

/**
 * 
 * @author admin
 *
 */
public interface UserEventRepository extends  ElasticsearchRepository<UserHistory, String> {
	
	public List<UserHistory> findByOrgiAndCreatetime(QueryBuilder query);
	
	public Page<UserHistory> findBySessionidAndOrgi(String sessionid , String orgi , Pageable page) ;
}
