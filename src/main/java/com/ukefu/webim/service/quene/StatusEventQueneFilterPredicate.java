package com.ukefu.webim.service.quene;

import java.util.Map;

import com.hazelcast.query.Predicate;
import com.ukefu.core.UKDataContext;
import com.ukefu.webim.web.model.StatusEvent;

public class StatusEventQueneFilterPredicate implements Predicate<String, StatusEvent> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1236581634096258855L;
	private String orgi ;
	/**
	 * 
	 */
	public StatusEventQueneFilterPredicate(String orgi){
		this.orgi = orgi ;
	}
	public boolean apply(Map.Entry<String, StatusEvent> mapEntry) {
		return orgi!=null && orgi.equals(mapEntry.getValue().getOrgi()) && UKDataContext.CallServiceStatus.INQUENE.toString().equals(mapEntry.getValue().getServicestatus()) ;
	}
}